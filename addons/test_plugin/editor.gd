tool
extends EditorPlugin

func _enter_tree():
	set_input_event_forwarding_always_enabled()

func forward_spatial_gui_input(cam,ev):
	base.cam_pos = cam.global_transform.origin
