extends Area

#lazy process control
const ratio = 40
const time = 2
const active_dist = 3
var active = false

export var action_id = int(1)
onready var focus_point = $lock

#actual process
const fade_min = 3

func action_requisites():
	print("requesites meet")
	return true

func _ready():
	# Get the viewport and clear it
	var viewport = get_node("Viewport")
	viewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)

	# Let two frames pass to make sure the vieport's is captured
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	# Retrieve the texture and set it to the viewport quad
	get_node("screen").material_override.albedo_texture = viewport.get_texture()
	set_process(false)
#	_lazy_process(1)
#
#func _process(delta):
#	var dist = (base.cam_pos-global_transform.origin).length()
#
#
#func _lazy_process(delay):
#	var dist = max(0.1,min((base.cam_pos-global_transform.origin).length(),ratio)/ratio)
#	delay = dist*time
#
#	var activate = dist<active_dist
#	if active_dist and !active:
#		set_process(true)
#	active = activate
#
#	yield(get_tree().create_timer(delay), "timeout")
#	_lazy_process(delay)
