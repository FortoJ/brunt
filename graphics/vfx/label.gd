#tool
extends Spatial
var tool_cam
var num
func _enter_tree():
	$view/Label.text = str(num)

func _ready():
	$anim.connect("animation_finished",self,"done")
	# Get the viewport and clear it
	var viewport = get_node("view")
	viewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
#	material_override = Material.new()

	# Let two frames pass to make sure the vieport's is captured
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
#	print(material_override)

	# Retrieve the texture and set it to the viewport quad
	$screen.material_override.albedo_texture = viewport.get_texture()
#	set_process(false)

func _process(delta):
#	print("cam is ",get_tree().get_edited_scene_root().get_node("samplecam").name)
	look_at(get_viewport().get_camera().global_transform.origin,Vector3.UP)

func done(ignore):
	queue_free()
