extends Node

var state = null setget set_state
var state_old = null

var state_db = {}
onready var parent = get_parent()

var enemy
func _ready():
	enemy = base.WORLD.get_node("player_base")

	for i in get_children():
		_add_state(i.name)
	call_deferred("set_state","dev")

#statemachine


#initiate machine
func _add_state(state_add):
	state_db[state_add] = get_node(state_add)
	



#processes
func _input(event):
	if state != null:
		state_db[state].handle_input(event)

func _physics_process(delta):
	if state != null:state_db[state].physics(delta)

func _process(delta):
	if state != null:state_db[state].logic(delta)

func anim_finished():
	if state != null:state_db[state].anim_finish(owner.anim)


#switcher
func set_state(new_state):
	if state == new_state:return
	state_old = state
	state = new_state

	if ![state_old,state].has(null):
		state_db[state].entering(state_old)
		state_db[state_old].exiting(state)


