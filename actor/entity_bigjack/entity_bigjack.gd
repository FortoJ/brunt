#tool
extends KinematicBody



#base.place_num(point,combo)
const G = -50
#var SPEED = 7
var SPEED = 12
var anim = ""
var anim_new = "idle"

var face_dir = 0
var at_left = false

var ld = Vector3()
var lv = Vector3()
var sight_point = Vector2(0,1)

var cam_dir
var navigation

var voice_db = {"none":null
			,"alert":preload("res://actor/entity_bigjack/voice/die-434129__89o__die.wav")
			,"lost":preload("res://actor/entity_bigjack/voice/lost.319362__monosfera__human-monster-01.wav")
}
var voice ="none"


const dir_db = [
			["fnsd",true]		#0	↖	(mirror)
			,["sd",true]		#1	←	(mirror)
			,["bksd",true]		#2	↙	(mirror)
			,["bk",false]		#3	↓
			,["bksd",false]		#4	↘
			,["sd",false]		#5	→
			,["fnsd",false]		#6	↗
			,["fn",false]		#7	↑
]

var health = 30

var sight : RayCast

func _enter_tree():
	sight = $sight


func _ready():
	navigation = base.current_nav
	lv = Vector3(0,0,2)
	$face_dir.rotation.y = 0.1
func _process(delta):
	var new_face = get_face()
	if voice != null:
		$voice.stream = voice_db[voice]
		$voice.play()
		voice = null
	

	if str(face_dir,anim) != str(new_face,anim_new):
		face_dir = new_face
		switch_anim()
#	if str(face_dir,anim) != str(new_face,anim_new):
#		face_dir = new_face
#		switch_anim()

func _physics_process(delta):
	lv = move_and_slide(lv, Vector3.UP)
	lv.y += delta * G





func is_in_fov(point):
	var rel_pos =(point-global_transform.origin)
	if rel_pos.length()<2.7:return true
	rel_pos = Vector2(rel_pos.x,rel_pos.z).normalized()

	return sight_point.dot(rel_pos)>.70


func can_see_point(point):
	var dist = point+Vector3(0,0.8,0)-sight.global_transform.origin
	sight.cast_to=dist#-(dist.normalized()*3)#+Vector3(0,.7,0)#-(dist.normalized()*4)
	sight.force_raycast_update()
	return !sight.is_colliding()

func get_face():
	if lv.length()>1:
		sight_point = Vector2(lv.x,lv.z).normalized()
#	cam_dir = $face_dir.global_transform.origin.direction_to(base.cam_pos)*Vector3(1,0,1)#get rotation relative to camera
	cam_dir = global_transform.origin.direction_to(base.cam_pos)*Vector3(1,0,1)#get rotation relative to camera
	$sprite.transform.basis =Transform().looking_at(-cam_dir,Vector3.UP).basis


	var mybas= $face_dir.global_transform.basis
	var dir = Vector2(mybas.z.dot(base.cam_basz),mybas.x.dot(base.cam_basz)).normalized().angle()

	dir = ((rad2deg(dir)+180)/360)+.95
	dir-=int(dir)
	return int(clamp(dir*8,0,7))

	
func switch_anim():
	var to_left = dir_db[face_dir][1]
	if at_left != to_left:
		$sprite.flip_h = to_left
		at_left = to_left
	var animation = str(anim_new+"_"+dir_db[face_dir][0])
	if anim == anim_new:
		var oldpos = $sprite.frame
		$sprite.animation = animation
		$sprite.frame = oldpos
	else:$sprite.animation = animation
#	$sprite.animation = str(anim_new+"_"+dir_db[face_dir][0])
	anim=anim_new
	
func switch_anim_bk():
	var to_left = dir_db[face_dir][1]
	if at_left != to_left:
		$sprite.flip_h = to_left
		at_left = to_left
#	$sprite.animation = str(anim_new+"_"+dir_db[face_dir][0])
	anim=anim_new



#interactions with the game
func hit_as_enemy(strength, who, kind):
	var hitstate = $StateMachine/get_hit
	var target = (global_transform.origin-who.global_transform.origin)*Vector3(1,0,1)
	hitstate.hurt_dir = target.normalized()#HERE
	hitstate.hurt_power = strength
	hitstate.kind = kind
	if $StateMachine.state == "get_hit":
		hitstate.new_hit()
	else:
		$StateMachine.set_state("get_hit")
#	print("I was in the ", point, " from ",who.name, " with strength of ", strength)

