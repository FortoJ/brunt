extends "res://system/statemachine_base.gd"

var sprite_anim


var hit_inertia = Vector3()
const MASS_VULNERABILITY = 0.3#higher == more vulnerable to punch push
var hurt_dir = Vector3()
var hurt_power

var face_dir
var kind
func _enter_tree():
#	sprite_anim = owner.get_node("sprite")
#	var frames =sprite_anim.frames
#	for ani in frames.get_animation_names():
#		print("ani", ani)
	face_dir = owner.get_node("face_dir")


func physics(delta):
#	if kind != "smash":return
#	return
#	print(owner.get_node("sprite").name)
#	print("hit inertia: ", hit_inertia.length())
	if hit_inertia.length()<=0.3:
		machine.set_state("chase")
		owner.lv = Vector3()
		return
	var vel = owner.lv as Vector3
	hit_inertia*=0.85
	var target = Vector3(hit_inertia.x,0,hit_inertia.y)
	
#	if vel.length() <1:
#		machine.set_state("chase")

#	owner.lv *=hit
#	var dir = owner.ld
#	var hvel = vel
#	hvel.y = 0
#
#	var target = dir * (owner.SPEED)

	var hvel = vel.linear_interpolate(hit_inertia, delta)
	vel.x = hvel.x
	vel.z = hvel.z
	
	
	owner.lv = vel
func logic(delta):
	var dir = owner.lv.normalized()
	
#	var rot = Vector2(dir.z,dir.x).angle()
#	var rot = Vector2(dir.x,dir.z).angle()
	var rot = Vector2(-dir.z,-dir.x).angle()
	face_dir.rotation.y = lerp_angle(face_dir.rotation.y,rot,0.7)

#func handle_input(ev):return ev

func new_hit():
	owner.health -=hurt_power
	base.place_num(owner.get_node("voice").global_transform.origin,owner.health)
	hit_inertia = hurt_dir*hurt_power*MASS_VULNERABILITY
	if owner.anim == "behit":
		owner.anim == ""
	owner.anim_new ="behit"


func entering(state_old):
	new_hit()
#	owner.anim_new ="behit"
#	hit_inertia = hurt_dir*hurt_power
	
#	base.place_num(point,combo)
#	yield(get_tree().create_timer(1.0), "timeout")
#	if machine.state== name:machine.set_state("chase")

#func exiting(state_next):pass
#func anim_finish(anim):
#	print("anim finished: ",anim)
