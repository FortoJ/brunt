extends "res://system/statemachine_base.gd"
var chasing = null
var cs_db = {#chase state db
	"init":0
	,"target_far":1
	,"target_close":2
	,"lost_sight":3
	,"done_chase":4
	,"failed_chase":5
}
var chase_state
const ACCEL= 20
const DEACCEL= 30
const HD = Vector3(1,0,1)#Horizontal Direction converter (use with "*")
var face_dir

var path = []
var dir = Vector3()

var onsight = false

const chase_commitment = 2
var chase_patience = 1

var last_ditch
#nodes
var sight : RayCast

func _enter_tree():
	sight = owner.get_node("sight")
#	sight.enabled = true
#	sight.enabled = false
	face_dir = owner.get_node("face_dir")

func physics(delta):
	var dist = chasing.global_transform.origin-sight.global_transform.origin+Vector3(0,.7,0)
#	sight.cast_to=dist
	sight.cast_to=Vector3().linear_interpolate(dist, .9)
	

	var hdir = (dir*HD).normalized()


	var vel = owner.lv as Vector3

	var hvel = vel
	hvel.y = 0

	var target = hdir * (owner.SPEED)
	var accel
	if hdir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, ACCEL * delta)
	
	vel.x = hvel.x
	vel.z = hvel.z
	owner.lv = vel

func logic(delta):
	
	if !owner.is_in_fov(chasing.global_transform.origin):
		onsight = false
	else:
		sight.force_raycast_update()
		onsight = !sight.is_colliding()


	var rot = Vector2(dir.z,dir.x).angle()
	face_dir.rotation.y = lerp_angle(face_dir.rotation.y,rot,0.7)

	if chase_state == cs_db["done_chase"]:
#		print("end by null chase")
		chase_state = null
		machine.set_state("idle")
		return
	if onsight:
		set_sight()
		chase_patience= chase_commitment
		last_ditch= null
	elif last_ditch != Vector3():
		last_ditch = chasing.global_transform.origin
#		print("updating last ditch: ",last_ditch)
	if path.size()<=0:
		
		if last_ditch != null:
			if last_ditch != Vector3():
				var mypos = owner.global_transform.origin
				var target = last_ditch
				path = owner.navigation.get_simple_path(mypos, target, true)
				path.remove(0)
				last_ditch = Vector3()
				return
		else:
			owner.voice = "lost"
			machine.step_stop()


#		dir = Vector3()
#		owner.lv = Vector3()
#		if chase_patience<=0:
#			owner.voice = "lost"
#			machine.step_stop()
#		else:owner.anim_new ="idle"
#		return


	if owner.is_on_wall() or owner.is_on_ceiling():
		pass
#		print("TODO: HIT UNSEXPECTED OBSTACLE")
	if path.size()<1:
		owner.voice = "lost"
		machine.step_stop()
		return
	dir = path[0]-owner.global_transform.origin
	if dir.length()<.5:
#		print("swich to next")
		path.remove(0)
		return



#func check_sight():
#	if machine.state != name:
#		#checksight turned off since nothing else will call it
#		return
#
#	if sight.is_colliding():
#		onsight = true
#	elif onsight:#not visible now, but was last time
#		set_sight()
#		onsight = false
#
#
#	yield(get_tree().create_timer(.3), "timeout")
#	check_sight()

func set_sight():
	var mypos = owner.global_transform.origin
	var target = chasing.global_transform.origin
	path = owner.navigation.get_simple_path(mypos, target, true)
	path.remove(0)

#func handle_input(ev):return ev
func entering(state_old):
	var mypos = owner.global_transform.origin
	var target = chasing.global_transform.origin
	path = owner.navigation.get_simple_path(mypos, target, true)
	chase_state = cs_db["init"]
	owner.anim_new ="run"
	chase_patience = chase_commitment
#	sight.add_exception(chasing)
#	check_sight()
	
#func exiting(state_next):



