extends "res://system/statemachine_base.gd"

var target_point = null



const ACCEL= 6
const DEACCEL= 10
var face_dir

func _enter_tree():
	face_dir = owner.get_node("face_dir")

func physics(delta):

	if target_point == null:
		machine.set_state("idle")
		return
	if owner.is_on_wall():
		target_point = null
		return


	var dir = (target_point-owner.global_transform.origin)
	if dir.length()<1.5:
		target_point = null
		return
	dir = dir.normalized()


	var vel = owner.lv as Vector3

	var hvel = vel
	hvel.y = 0

	var target = dir * (owner.SPEED)
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, ACCEL * delta)
	
	vel.x = hvel.x
	vel.z = hvel.z
	owner.lv = vel

func logic(delta):
	var dir = owner.lv.normalized()
	var rot = Vector2(dir.z,dir.x).angle()
	face_dir.rotation.y = lerp_angle(face_dir.rotation.y,rot,0.7)
	

#func handle_input(ev):return ev
func entering(state_old):
	owner.anim_new ="run"
#func exiting(state_next):pass
#func anim_finish(anim):pass


