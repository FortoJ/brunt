extends "res://system/statemachine_base.gd"

var roster = []

func physics(delta):
	var vel = owner.lv as Vector3
	
	machine.base_input()
	if owner.ld != Vector3():
		if vel.length() >1:
			machine.set_state("run")
		vel.x = min(owner.SPEED,max(owner.ld.x,-owner.SPEED))
		vel.z = min(owner.SPEED,max(owner.ld.z,-owner.SPEED))
	else:
		vel.x *=0.4
		vel.z *=0.4

		
	owner.lv = vel


#	var dist = chasing.global_transform.origin-sight.global_transform.origin+Vector3(0,.7,0)
##	sight.cast_to=dist
#	sight.cast_to=Vector3().linear_interpolate(dist, .9)

func update_roster():
	var rostersize =base.ingame_players.size()
	roster = []
	for p in base.ingame_players:
		roster.append(p)
		owner.sight.add_exception(base.ingame_players[p]["node"])
	if  rostersize == 1:
		find_annoying_players(null)
	else:
		find_annoying_players(rostersize)


func find_annoying_players(num):
	if machine.state != "idle":return
	var player
	if num == null:#check
		if base.ingame_players[roster[0]] == null:
			update_roster()
			return
		player = base.ingame_players[roster[0]]
	else:
		if num >=base.ingame_players.size():
			num = 0
		if base.ingame_players[roster[0]] == null:
			update_roster()
			return
		player = base.ingame_players[roster[num]]
		num+=1
#		if num
#		#KEEP WORKING FROM HERE
#{"pos":Vector3(),"node":self}
#	print("testing: ", player["node"].name," currently at: ",player["pos"])


	if owner.is_in_fov(player["pos"]):

		if owner.can_see_point(player["pos"]):
			owner.voice ="alert"
			machine.state_db["chase"].chasing =player["node"]
			machine.set_state("chase")
			return
	
	yield(get_tree().create_timer(.1), "timeout")
	find_annoying_players(num)


func logic(delta):
	pass

#	print(owner.get_node("face_dir").rotation.y)
#	print("infov:" ,owner.is_in_fov(base.cam_pos))
#	pass
#	print(owner.get_node("face_dir").rotation)
#	print(owner.cam_dir)
#	var a = owner.get_node("ai").enemy.global_transform.origin
#	var b = owner.global_transform.origin
#	var direction = Vector2(a.x-b.x,a.z-b.z).angle()
##	var direction = (owner.get_node("ai").enemy.global_transform.origin-owner.global_transform.origin).normalized()
#	print(direction,".",owner.get_node("face_dir").transform.basis.y)
#
#func handle_input(ev):return ev
#
func entering(state_old):
	for p in base.ingame_players:
		roster.append(p)
		owner.sight.add_exception(base.ingame_players[p]["node"])
#	print("enter in idle")
	owner.anim_new ="idle"
	update_roster()
#	
#func exiting(state_next):pass
#
#func anim_finish(anim):pass
#
