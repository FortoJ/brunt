tool
extends KinematicBody

var cam_pos = [Vector2(),0]

var anim = ""
var anim_new = "idle"


var my_dir = 0
var face_dir = 0

var at_left = false

const dir_db = [
			["sd",false]		#0	→
			,["bksd",false]		#1	↗
			,["bk",false]		#2	↑
			,["bksd",true]		#3	↖	(mirror)
			,["sd",true]		#4	←	(mirror)
			,["fnsd",true]		#5	↙	(mirror)
			,["fn",false]		#6	↓
			,["fnsd",false]		#7	↘
]

func _process(delta):
	$sprite.rotate_y(delta)
#	if Engine.editor_hint:
#		if cam_pos != base.cam_pos:
#			cam()
#		else:cam()
#		cam_pos = base.cam_pos
#	else:cam()
	
	

	var new_face = get_face()
	if face_dir != new_face:
		face_dir = new_face
		switch_anim()
	elif anim != anim_new:switch_anim()


func get_face():
	var my_dir = ($sprite.rotation.y+3.14)/6.28#get own rotation, convert to float 0.0~1.0
	var cam_dir = $sprite.global_transform.origin.direction_to(base.cam_pos)#get rotation relative to camera
	cam_dir = (Vector2(cam_dir.x,cam_dir.z).angle()+3.14)/6.28#convert to float 0.0~1.0
	var direction = my_dir+cam_dir+.6#sum both rotations, add slight rotation
	return int((direction-int(direction))*8)#convert to float 0.0~1.0, then integer split in 8 directions

func switch_anim():
	var to_left = dir_db[face_dir][1]
	if at_left != to_left:
		var sprite_scale = $sprite.scale.y
		$sprite.flip_h = to_left
		at_left = to_left
	$sprite.animation = str(anim_new+"_"+dir_db[face_dir][0])
	anim=anim_new
