extends Node





var state = null setget set_state
var state_old = null

var state_db = {}
onready var parent = get_parent()

#initiate machine
func _add_state(state_add):
	state_db[state_add] = get_node(state_add)
	
func _enter_tree():
	pass
#	owner.get_node("sprite").connect("animation_finished",self,"anim_finished")
	
func _ready():
	for i in get_children():
		_add_state(i.name)
		
	call_deferred("set_state","reachpoint")
	$reachpoint.target_point =owner.global_transform.origin+Vector3(1,0,0)
#	call_deferred("set_state","run")
#	yield(get_tree().create_timer(.1), "timeout")
#	call_deferred("set_state","idle")


#processes
func _input(event):
	if state != null:
		state_db[state].handle_input(event)

func _physics_process(delta):
	if state != null:state_db[state].physics(delta)

func _process(delta):
	if state != null:state_db[state].logic(delta)

func anim_finished():
	if state != null:state_db[state].anim_finish(owner.anim)


#switcher

func step_stop():#do a step in random direciton, then idle
	var dest = owner.global_transform.origin
	var rand = Vector2(1,0).rotated(randf())*1.5
	dest+=Vector3(rand.x,0,rand.y)
	state_db["reachpoint"].target_point = dest
	set_state("reachpoint")



func set_state(new_state):
	if state == new_state:return
	state_old = state
	state = new_state

	if ![state_old,state].has(null):
		state_db[state].entering(state_old)
		state_db[state_old].exiting(state)




#mechanics helper

func base_input():
	owner.ld = Vector3(Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),0,Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up"))
#	print(owner.ld)
	
