extends Node
#the mole is a special node that gives relevant info to player's enemy (betray process)


var status = false

#tell the (chasing) enemy if the player is running away (enemy chase and attack whenver got chance)
#or is standing nearby (enemy sorround player and look wait for their ally to join the fight)
var stationary = false
var speed = [0,1]#sum and timer

var oldpos = Vector3()

func _process(delta):
	var new_pos = owner.global_transform.origin*Vector3(1,0,1)
	speed[0] += (new_pos-oldpos).length()*delta
	speed[1] -= delta
	if speed[1]<=0:
		stationary = speed[0]<0.5
		
		speed = [0,4]
#	print(speed)
	

	oldpos = new_pos

func set_on():
	status = true
	set_process(true)
func set_off():
	status = false
	set_process(false)
