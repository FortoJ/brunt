extends Node

var state = null setget set_state
var state_old = null

var state_db = {}
onready var parent = get_parent()


#nodes
onready var pitch = get_node("../pitch")
onready var checkgrip = owner.checkclimbray as RayCast

var switch_state_delay = null

func _enter_tree():
	owner.get_node("ui/arms/sprite").connect("animation_finished",self,"animarm_finished")
	for i in get_children():
		_add_state(i.name)
	
#initiate machine
func _add_state(state_add):
	state_db[state_add] = get_node(state_add)
	


func _ready():
	call_deferred("set_state","idle")


#processes
func _input(event):
	if state != null:
		state_db[state].handle_input(event)

func _physics_process(delta):
	if state != null:state_db[state].physics(delta)
	if switch_state_delay != null:
		switch_state_delay[1] -=delta
		if switch_state_delay[1]<=0:
			set_state(switch_state_delay[0])
			switch_state_delay = null

func _process(delta):
	if state != null:state_db[state].logic(delta)
	

func animarm_finished():
	if state != null:state_db[state].animarm_finished(owner.animarm)


#switcher


func stagger(how_long,return_state):
	$stagger.TO = how_long
	$stagger.next_state = return_state
	set_state("stagger")

func set_state(new_state, delay = null):
	if state == new_state:return
	if delay != null:
		if switch_state_delay != null:#first check there's not another request
			if switch_state_delay[0] == new_state:#we're already waiting for you, be patient
				return
		#it's a new request
		switch_state_delay = [new_state,delay]
		return
	state_old = state
	state = new_state
	switch_state_delay = null

	if ![state_old,state].has(null):
		state_db[state].entering(state_old)
		state_db[state_old].exiting(state)




#mechanics helper


	
func basic_input(event):
	if event.get_class() == "InputEventMouseMotion":
		var mouse_motion = event.relative*base.MOUSE_SPEED
		owner.rotation.y -= mouse_motion.x

		if mouse_motion.y != 0:
			owner.pitching = clamp(owner.pitching-mouse_motion.y,owner.PITCH_LIMIT.x,owner.PITCH_LIMIT.y)
			pitch.rotation.x = owner.pitching
			adjust_sight()

func air_movment(vel,key):

	var dir = get_walk_dir(owner.transform.basis,key)
	var hvel = vel
	hvel.y = 0

	var target = dir * (owner.SPEED)

	hvel = hvel.linear_interpolate(target,0.04)
	
	vel.x = hvel.x
	vel.z = hvel.z
	return vel

func check_action(type = "soft"):#used by idle and walk states
	if !owner.action_ray.is_colliding():return false#colliding with anything abort
	var target_node =owner.action_ray.get_collider()
	if !target_node.has_method("action_requisites"):return false#it's colliding, but not what we're looking for
	var requesites_meet = target_node.action_requisites()
	if !requesites_meet:return false#it's colliding, it's what are we looking for but... it said no! :/
	$cine_action.type = type
	$cine_action.id = target_node.action_id
	$cine_action.focus_point = target_node.focus_point.global_transform.origin
	set_state("cine_action")
	return true
	
func adjust_sight(center = null):
	var arm_y_posrange = [owner.arm_y_posrange[0]*base.current_ratio.y,owner.arm_y_posrange[1]*base.current_ratio.y]
	if center:
		owner.get_node("ui/arms").rect_global_position.y = (arm_y_posrange[1])
		return
	var limits = clamp(owner.pitching, -2,1.6)
	var value = inverse_lerp(0, -1.2, limits)
	
	var fin = (value*(arm_y_posrange[1]-arm_y_posrange[0]))+arm_y_posrange[0]
	owner.get_node("ui/arms").rect_global_position.y = fin

func check_grab():
	if !checkgrip.is_colliding():return false
	if owner.checkclimbspace.get_overlapping_bodies().size() == 1:return true


func get_walk_dir(aim,dir):
	return (dir.x*aim[0])+(dir.y*aim[2]).normalized()

