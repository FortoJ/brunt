extends "res://system/statemachine_base.gd"

#var floor_vel = Vector3()
var floor_pos = Vector3()
var walktowall = false
func physics(delta):
	var vel = owner.vel
#	if owner.onground == null:
#	if !owner.is_on_floor():
	if !owner.get_ground():
			machine.set_state("fall")
			return
	if Input.is_action_pressed("atk_alt"):
		if machine.get_node("punch_chrg").ready:
			machine.set_state("punch_chrg")
			return

#	var floor_nowpos = owner.onground.transform.origin
#	owner.transform.origin += floor_nowpos-floor_pos
#	vel += floor_nowpos-floor_pos
	#floor_pos = floor_nowpos
	if owner.key_direction != Vector2() and !walktowall:
		
#		if vel.length()>4:
		if (vel*Vector3(1,0,1)).length()>2:
#			print("too fast2walk ",(vel*Vector3(1,0,1)).length())
#			print(vel.length())
			machine.set_state("walk")
			return

		var dir = machine.get_walk_dir(owner.transform.basis,owner.key_direction)
		vel += dir*owner.SPEED*delta*3
	else:
		
		vel*=Vector3(0.3,1,0.3)

#	if Input.is_action_just_pressed("jump") and owner.onground != null:
#	if Input.is_action_just_pressed("jump") and owner.is_on_floor():
	if Input.is_action_just_pressed("jump") and owner.get_ground():
		vel.y += owner.JUMP
		machine.set_state("jump")
#		return
	elif Input.is_action_just_pressed("atk_main"):
		if machine.check_action("hard"):#there's an action
			return
		else:
			machine.set_state("punch")
			owner.vel = Vector3()
			return
	
	owner.vel = vel
	
#	floor_pos = floor_nowpos
func logic(delta):
	if Input.is_action_just_pressed("action"):
		if machine.check_action():return
	if owner.sfx == "none" and owner.get_node("sfx").is_playing():
		owner.get_node("sfx").stop()
func handle_input(ev):
	machine.basic_input(ev)

func entering(state_old):
#	if state_old == "walk" and owner.get_node("checkclimbspace/checkgrip").is_colliding():
	if !walktowall:
		if state_old == "walk" and owner.is_on_wall():
			walktowall = true
			owner.animarm_new = "wallblock"
			owner.animhead_new = "hitwall"
			owner.voice = "gasp"
			yield(get_tree().create_timer(0.4), "timeout")
			walktowall = false
			return
		if state_old == "fall":
#			print("step form fall")
			owner.animarm_new = "idle"
			owner.animhead_new = "landground"
			owner.sfx_new = owner.get_step_sfx()
			yield(get_tree().create_timer(0.2), "timeout")
			if owner.sfx != "none":
				owner.sfx_new ="none"

			return
	owner.animarm_new = "idle"
	owner.animhead_new = "idle"
	owner.sfx_new ="none"
#
#func exiting(state_next):

func animarm_finished(anim):
	if anim == "wallblock":
		owner.animarm_new = "idle"
	

