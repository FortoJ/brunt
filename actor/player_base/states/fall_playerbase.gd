extends "res://system/statemachine_base.gd"

func physics(delta):
	if machine.check_grab() and (owner.climb_ready):
		machine.set_state("climb")

	var vel = owner.vel
	
#	if owner.onground!=null:
#	if owner.is_on_floor():
	if owner.get_ground():
		var speed = Vector2(vel.x,vel.z).length()
		if speed > 5:
			machine.set_state("walk")
		else:
			machine.set_state("idle")
		owner.vel = vel
		return

	var key = owner.key_direction
	if key != Vector2():vel = machine.air_movment(vel,key)
	else:vel = machine.air_movment(vel,key)

#	if key != Vector2():
#
#		var dir = machine.get_walk_dir(owner.transform.basis,key)
#		var hvel = vel
#		hvel.y = 0
#
#		var target = dir * (owner.SPEED)
#		var accel
#		if dir.dot(hvel) > 0:
#			accel = ACCEL
#		else:
#			accel = DEACCEL
#
#		hvel = hvel.linear_interpolate(target, ACCEL * delta)
#
#		vel.x = hvel.x
#		vel.z = hvel.z

	owner.vel = vel


#func logic(delta):return delta
#
func handle_input(ev):machine.basic_input(ev)
#
func entering(state_old):
	owner.sfx_new ="none"
	owner.animarm_new= "jump-fall"
	owner.animhead_new = "idle"
#
#func exiting(state_next):pass
#
func anim_finish(anim):
	if anim == "jump-fall":
		owner.animarm_new= "onair"
	


