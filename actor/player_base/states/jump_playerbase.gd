extends "res://system/statemachine_base.gd"


var on_air = false
func physics(delta):
	if machine.check_grab() and (owner.climb_ready):
		machine.set_state("climb")

	var vel = owner.vel
	var onfloor = owner.get_ground()
	if !on_air and !onfloor:#we really start when player is actually in air
		on_air = true
		return
	
	if onfloor and on_air:
		var speed = Vector2(vel.x,vel.z).length()
		if speed > 5:
			machine.set_state("walk")
		else:
			machine.set_state("idle")
		owner.vel = vel
		return
	if vel.y < 0:
		machine.set_state("fall")
	var key = owner.key_direction
	if key != Vector2():vel = machine.air_movment(vel,key)
	else:vel = machine.air_movment(vel,key)

	owner.vel = vel
func entering(state_old):
	owner.sfx_new ="none"

	if state_old == "idle" or state_old == "walk":
		on_air = false
		owner.animarm_new= "jump-landoff"
		owner.animhead_new = "idle"
	owner.voice = str("jump",randi()%3+1)#jump1, jump2 or jump3

func handle_input(ev):machine.basic_input(ev)
