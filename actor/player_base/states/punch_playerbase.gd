extends "res://system/statemachine_base.gd"

const COMBO_TO = 1
var combo
var combo_timer
var combo_db = {
			1:["punch-hit",false,.3]
			,2:["punch-combo1",false,.2]
			,3:["punch-combo2",false,.2]
			,4:["punch-combo3",false,1]
}
var animated_arms
var punch_delay_recover = null
func _enter_tree():
	animated_arms = owner.get_node("pitch/neck/anim")
	combo_timer = Timer.new()
	add_child(combo_timer)
	combo_timer.connect("timeout",self,"combo_timeout")

func physics(delta):
	if punch_delay_recover != null:
		punch_delay_recover-=delta
		if punch_delay_recover<=0:punch_delay_recover = null
	elif Input.is_action_just_pressed("atk_main"):
		if combo == null:
			punch_delay_recover = combo_db[1][2]
		else:
			punch_delay_recover = combo_db[combo+1][2]
		var ray = owner.action_ray as RayCast
		if ray.is_colliding():
			var target = ray.get_collider()
			if target.owner.has_method("hit_as_enemy"):
				hit(target,ray.get_collision_point())
			else:
				miss()
				return
		else:miss()

#	print(combo_timer.time_left)
#func handle_input(ev):return ev


func hit(target,point):
	var pwr = owner.punch_pwr
	var head ="shake-low"
	var hands = "punch-hit"
	var kind = "punch"
	var sfx = "hitbody"
	combo_timer.start(COMBO_TO)
	if combo == null:
		combo = 1

	else:
		combo+= 1
		hands = combo_db[combo][0]
		base.place_num(point,combo)
		if combo == 4:
			head = "smash"
			kind = "smash"
			combo = null
			pwr*=2
			machine.stagger(.5,"idle")

	base.vfx(point,"hityellow1")
	target.owner.hit_as_enemy(pwr, owner, kind)
	owner.animhead_new = head
	owner.animarm_new = hands
	owner.sfx = ""
	owner.sfx_new = sfx

func miss():
	owner.animarm_new= "punch-miss"
	owner.sfx_new ="swoosh"
	owner.animhead_new = "punch"
	machine.adjust_sight(true)


func entering(state_old):
	var ray = owner.action_ray as RayCast
	var hit

	if ray.is_colliding():
		var target = ray.get_collider()
		if target.owner.has_method("hit_as_enemy"):
			hit(target,ray.get_collision_point())
		else:
			miss()
			return
	else:miss()
	

func exiting(state_next):
	machine.adjust_sight()


func animarm_finished(anim):
	machine.set_state("idle")
func combo_timeout():
	combo_timer.stop()
	combo = null
