extends "res://system/statemachine_base.gd"

var TO#timeout
var next_state



func logic(delta):
	TO -=delta
	if TO <=0:
		machine.set_state(next_state)
		TO = null
		next_state = null


func entering(state_old):
	if TO == null:
		TO = 0
	if next_state == null:
		next_state =  "idle"
