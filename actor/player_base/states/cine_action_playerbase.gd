extends "res://system/statemachine_base.gd"

var type#soft or hard
var id
var focus_point

onready var cam_holder = owner.get_node("pitch/cam")

var arm_pos


func _enter_tree():
	arm_pos=owner.arm_y_posrange[1]*1.1

func _ready():
	print(cam_holder.name)
#func physics(delta):return delta

func logic(delta):
	var orig = cam_holder.global_transform
	var target = orig.looking_at(focus_point,Vector3.UP)
	cam_holder.global_transform.basis = orig.basis.slerp(target.basis,0.3)


	var arm_loc = lerp(owner.get_node("ui/arms").rect_global_position.y,arm_pos,.8)
	owner.get_node("ui/arms").rect_global_position.y = arm_loc




#func handle_input(ev):return ev
func entering(state_old):
	if type == "hard":
		owner.animarm_new= "button_smash"
	else:
		owner.animarm_new= "button_press"
	
#func exiting(state_next):pass
func animarm_finished(anim):
	machine.set_state("idle")
