extends "res://system/statemachine_base.gd"



const ACCEL= 6
const DEACCEL= 10

const DELAY = 0.7
var ready = true

var arm_pos

var groundsfx_db


func _enter_tree():
	arm_pos=owner.arm_y_posrange[1]*1.1

func _ready():
	groundsfx_db = machine.state_db["walk"].groundsfx_db
func physics(delta):
	var vel = owner.vel


#	if owner.onground == null:
#	if !owner.is_on_floor():
	if !owner.get_ground():
		machine.set_state("fall")
		return
	if !Input.is_action_pressed("atk_alt"):
		machine.set_state("idle")
		
	if owner.action_ray.is_colliding():
		var target = owner.action_ray.get_collider()
		if target.has_method("hit_as_enemy"):
			owner.animarm_new= "readypunch-smash"
			yield(get_tree(), "idle_frame")
			vel = Vector3()
			owner.vel = vel
			machine.set_state("punch")
			return
	var key = owner.key_direction


	if key != Vector2():
		
		var dir = machine.get_walk_dir(owner.transform.basis,key)
		var hvel = vel
		hvel.y = 0
	
		var target = dir * (owner.SPEED*.6)
		var accel
		if dir.dot(hvel) > 0:
			accel = ACCEL
		else:
			accel = DEACCEL
	
		hvel = hvel.linear_interpolate(target, ACCEL * delta)
		
		vel.x = hvel.x
		vel.z = hvel.z

	else:vel*=Vector3(DEACCEL*delta,1,DEACCEL*delta)
	owner.vel = vel

func logic(delta):
	var hvel = (owner.vel*Vector3(1,0,1)).length()
	if owner.animarm == "readypunch-gettop":
		var arm_loc = lerp(owner.get_node("ui/arms").rect_global_position.y,arm_pos,.8)
		owner.get_node("ui/arms").rect_global_position.y = arm_loc
		return
	
	if hvel <1:
		owner.animhead_new = "idle"
		owner.sfx_new ="none"
	elif !groundsfx_db.has(owner.current_ground_type):
		owner.animhead_new = "walkcharged"
		owner.sfx_new ="step_base"
	else:
		owner.animhead_new = "walkcharged"
		owner.sfx_new =groundsfx_db[owner.current_ground_type]

func handle_input(event):
	if event.get_class() == "InputEventMouseMotion":
		var mouse_motion = event.relative*base.MOUSE_SPEED
		owner.rotation.y -= mouse_motion.x

		if mouse_motion.y != 0:
			owner.pitching = clamp(owner.pitching-mouse_motion.y,owner.PITCH_LIMIT.x,owner.PITCH_LIMIT.y)
			machine.pitch.rotation.x = owner.pitching
#			machine.adjust_sight(true)

func animarm_finished(anim):
#	print(anim)
	if anim == "readypunch-gettop":
#		print("readypunch")
		owner.animarm_new= "readypunch"

func exiting(state_next):
	machine.adjust_sight()
	yield(get_tree().create_timer(DELAY), "timeout")
	ready = true


func entering(state_old):
	ready = false
	owner.sfx_new ="none"
	owner.animhead_new = "idle"
	owner.animarm_new= "readypunch-gettop"
#	owner.animhead_new = "run"
