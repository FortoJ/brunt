extends "res://system/statemachine_base.gd"

const CLIMB_SPD =10

var grabray = RayCast
var climb_height = 3
var reach_target = 0
var ground = 0
var direction

func _enter_tree():
	climb_height = owner.checkclimbspace.get_node("area").shape.extents.y
	climb_height *=1.4
	grabray = owner.get_node("checkclimbspace/checkgrip")

func physics(delta):
	var vel = owner.vel
	var rot = owner.rotation.y
	rot = lerp_angle(rot, direction, 0.1)
	owner.rotation.y = rot
	
	vel.y +=(CLIMB_SPD-base.G)*delta
	if owner.global_transform.origin.y >= ground:
		machine.set_state("fall")
		owner.vel = Vector3()
		var dir = machine.get_walk_dir(owner.transform.basis,Vector2(0,-1))*owner.SPEED*.2
		dir.y = owner.JUMP*.4
		vel = Vector3()
		owner.vel = dir
		return
	owner.vel = vel

#func logic(delta):return delta

#func handle_input(ev):return ev

# warning-ignore:unused_argument
func entering(state_old):
#	print("climbing")
#	print("speed in climb : ",owner.vel.y)
	if !grabray.is_colliding():
#		print("fail for not colliding")
		machine.set_state("fall")
		
		return
	var col = grabray.get_collision_normal()
	direction= atan2(col.x,col.z)
	owner.climb_ready = false
	var heigth = climb_height
	var fall_sped = owner.vel.y
	if fall_sped <0:#if player was falline, we need to give more
						#power/height to reacht the spot
		var extra = abs(max(-20,fall_sped))*.025
#		print("on owner: ", owner.vel.y, "we add: ", extra)
		heigth+=extra
	owner.vel = Vector3()
	ground = (owner.global_transform.origin.y+heigth)
#	print("height: ", climb_height, "tot: ",ground)
#	print("ground will be:",ground)
	owner.animarm_new= "climb"
	owner.animhead_new = "climb"
	owner.sfx_new ="grab"
#	owner.voice = str("jump",randi()%3+1)#jump1, jump2 or jump3
	owner.voice = str("attack",randi()%3+1)
	yield(get_tree().create_timer(2.0), "timeout")

	owner.climb_ready = true


func anim_finish(anim):
	if anim == "climb":
		owner.animarm_new= "jump-fall"

func exiting(state_next):
	if owner.animarm == "climb":
		owner.animarm_new= "jump-fall"

