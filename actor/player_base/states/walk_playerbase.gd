extends "res://system/statemachine_base.gd"

const ACCEL= 6
const DEACCEL= 10

var ground_delay = 0

var groundsfx_db = {"grass":"step_grass"
					,"cobble":"step_cobble"
					,"metal":"step_metal"
}
func physics(delta):
	var vel = owner.vel


#	if owner.onground == null:
#	if !owner.is_on_floor():
	if !owner.get_ground():
			ground_delay -=delta
			if ground_delay <=0:
				machine.set_state("fall")
				return
#			machine.set_state("fall",0.3)
#	else:ground_delay = 0.25
	else:ground_delay = 0.15
	if (vel*Vector3(1,0,1)).length()<=2:
		machine.set_state("idle")
		return
	if Input.is_action_pressed("atk_alt"):
		if machine.get_node("punch_chrg").ready:
			machine.set_state("punch_chrg")
			return

	var key = owner.key_direction


	if key != Vector2():
		
		var dir = machine.get_walk_dir(owner.transform.basis,key)
		var hvel = vel
		hvel.y = 0
	
		var target = dir * (owner.SPEED)
		var accel
		if dir.dot(hvel) > 0:
			accel = ACCEL
		else:
			accel = DEACCEL
	
		hvel = hvel.linear_interpolate(target, ACCEL * delta)
		
		vel.x = hvel.x
		vel.z = hvel.z

	else:vel*=Vector3(DEACCEL*delta,1,DEACCEL*delta)

	if Input.is_action_just_pressed("jump"):# and owner.is_on_floor():
		var JUMPWR = owner.JUMP
		if vel.y <0:#if player is going down, compensate by adding jump power
			JUMPWR-=max(-6,vel.y)
		vel.y += JUMPWR
		machine.set_state("jump")
	elif Input.is_action_just_pressed("atk_main"):
		if machine.check_action("hard"):#there's an action
			return
		else:
			machine.set_state("punch")
			owner.vel = Vector3()
			return
	
#	elif Input.is_action_just_pressed("atk_main"):
#		machine.set_state("punch")
#		owner.vel = Vector3()
#		return

	owner.vel = vel

#
func logic(delta):
	var key =owner.key_direction
#	print(owner.current_ground_type)
	if key.x != 0:
		if key.x <0:owner.animarm_new="runside_l"
		else:owner.animarm_new="runside_r"
	else:owner.animarm_new= "run"
	owner.sfx_new = owner.get_step_sfx()
#	if !groundsfx_db.has(owner.current_ground_type):
#		owner.sfx_new ="step_base"
#	else:
#		owner.sfx_new =groundsfx_db[owner.current_ground_type]

	
func handle_input(ev):
	machine.basic_input(ev)
#	if Input.is_action_just_pressed("jump") and owner.is_on_floor():
#		owner.vel.y += owner.JUMP

#
func entering(state_old):
	owner.sfx_new ="step_soft"
	owner.animarm_new= "run"
	owner.animhead_new = "run"

func exiting(state_next):
	if ["jump","idle","fall"].has(state_next):owner.sfx_new = "none"
#
#func anim_finish(anim):pass
#
