extends KinematicBody

#physics
onready var G = base.G
var SPEED = 13
#var JUMP = 10
var JUMP = 15
var unslide = false

var vel = Vector3()
var onground = false
#camera settings
var pitching = 0
const PITCH_LIMIT = Vector2(-1.6,1.6)
const arm_y_posrange = [480,330]#mid keep this maybe
onready var action_ray = $pitch/cam/ray

#input
var key_direction = Vector2()

#mechanics
const GRAB_DELAY = 0.05
var climb_cooldown = 0

#physical world info
var current_ground_type

#animations
var animarm = ""
var animarm_new = "idle"
var animhead = ""
var animhead_new = "idle"

#sfx
var sfx = ""
var sfx_new = ""
var floor_kind = "ground"
#voice
var voice = null
#var voice_new =""

#var punch_pwr = 10
var punch_pwr = 40


#climb mechanics
var checkclimbspace
var checkclimbray
var climb_ready = true

var my_id

#sfx
var sfx_db = {"none":null
			,"step_base":preload("res://actor/player_base/sfx/step_base.167685__cris__footsteps-in-a-concrete-corridor-1.wav")
			,"step_cobble":preload("res://actor/player_base/sfx/step_next.465360__teemophey__.wav")
			,"step_soft":preload("res://actor/player_base/sfx/step_soft.474051__sheyvan__steps-shoes-on-carpet-compilation.wav")
			,"step_grass":preload("res://actor/player_base/sfx/step_grass.180746__martian__woodland-leafy-step-bys-various-2-twig-snaps-m.wav")
			,"step_metal":preload("res://actor/player_base/sfx/step_metal.398937__mypantsfelldown__metal-footsteps.wav")
			,"grab":preload("res://actor/player_base/sfx/grab.174638__altfuture__leather-stretched.wav")
			,"swoosh":preload("res://actor/player_base/sfx/swoosh.wav")
			,"hitbody":preload("res://actor/player_base/sfx/punchhit.hit17.wav")
}
var voice_db = {"none":null
			,"1":preload("res://actor/player_base/voice/anime_ah-cicifyre.wav")
			,"2":preload("res://actor/player_base/voice/anime_wah-cicifyre.wav")
			,"attack1":preload("res://actor/player_base/voice/attack1-cicifyre.wav")
			,"attack2":preload("res://actor/player_base/voice/attack2-cicifyre.wav")
			,"5":preload("res://actor/player_base/voice/attack3b-cicifyre.wav")
			,"attack3":preload("res://actor/player_base/voice/attack3-cicifyre.wav")
			,"grab":preload("res://actor/player_base/voice/damaged1b-cicifyre.wav")
			,"8":preload("res://actor/player_base/voice/damaged1-cicifyre.wav")
			,"9":preload("res://actor/player_base/voice/damaged2b-cicifyre.wav")
			,"10":preload("res://actor/player_base/voice/damaged2-cicifyre.wav")
			,"11":preload("res://actor/player_base/voice/damaged3b-cicifyre.wav")
			,"12":preload("res://actor/player_base/voice/damaged3-cicifyre.wav")
			,"gasp":preload("res://actor/player_base/voice/gasp1-cicifyre.wav")
			,"14":preload("res://actor/player_base/voice/healed1-cicifyre.wav")
			,"15":preload("res://actor/player_base/voice/healed2-cicifyre.wav")
			,"16":preload("res://actor/player_base/voice/healed3-cicifyre.wav")
			,"17":preload("res://actor/player_base/voice/how3_ouch-cicifyre.wav")
#			,"jump1":preload("res://actor/player_base/voice/jump1b-cicifyre.wav")
			,"jump1":preload("res://actor/player_base/voice/jump1-cicifyre.wav")
#			,"jump3":preload("res://actor/player_base/voice/jump2b-cicifyre.wav")
			,"jump2":preload("res://actor/player_base/voice/jump2-cicifyre.wav")
#			,"jump5":preload("res://actor/player_base/voice/jump3b-cicifyre.wav")
			,"jump3":preload("res://actor/player_base/voice/jump3-cicifyre.wav")
}


var groundsfx_db = {"grass":"step_grass"
					,"cobble":"step_cobble"
					,"metal":"step_metal"
}

#nodes
var mole

func _enter_tree():
	mole =$mole
	my_id = randi()
	base.ingame_players[my_id] = {"pos":Vector3(),"node":self}
	checkclimbspace = $checkclimbspace
	checkclimbray = $checkclimbspace/checkgrip
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().connect("screen_resized",self,"resize")

func _ready():
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	resize()

func resize():
	yield(get_tree(), "idle_frame")
	var y_ratio = base.screen_size.y/base.base_size.y
	$ui/arms.rect_scale = base.current_ratio*1.1
	$ui.rect_position = base.screen_size/2
	$StateMachine.adjust_sight()

func _process(delta):
	base.ingame_players[my_id]["pos"] = global_transform.origin
	key_direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	key_direction.y = Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward")
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	if Input.is_action_just_pressed("debug_a"):
		animhead_new = "shake-low"
#		base.place_locator(global_transform.origin)
#		if mole.status:
#			mole.set_off()
#		else:mole.set_on()
#		$StateMachine.set_state("fall")
#		print(owner.get_node("pitch/checkclimbspace/area").shape)
#		print(owner.name)#/checkclimbspace/area").shape)
	if Input.is_action_just_pressed("debug_b"):
		$StateMachine.set_state("climb")

	if animarm != animarm_new:switcharm()

	if animhead != animhead_new:
		$pitch/neck/anim.play(animhead_new)
		animhead = animhead_new
	
	if sfx != sfx_new:
		if $sfx.is_playing():
			$sfx_dryout.volume_db = base.aud_sfx
			$sfx_dryout.stream = $sfx.stream
			$sfx_dryout.play($sfx.get_playback_position())
		

		
		
		if sfx_new == "none":$sfx.stop()
		else:
			$sfx.stream = sfx_db[sfx_new]
			$sfx.play()
		sfx = sfx_new
	if voice != null:
		$voice.stream = voice_db[voice]
		$voice.play()
		voice = null
#debug stuff
#	var text = str($pitch/checkgrip.global_transform.basis.get_euler().y,"\n")
#	if $pitch/checkgrip.is_colliding():
#		var col = $pitch/checkgrip.get_collision_normal()
##		col = Vector2(col.x,col.y).angle()
#		text+=str(atan2(col.x,col.z))
#	text+=str("\n",rotation.y)
#	$info.text = str((vel*Vector3(1,0,1)).length())


		
	$info.text = str(sfx, "\n" ,sfx_new, "\n",$sfx_dryout.is_playing())
	if $sfx_dryout.is_playing():
		$sfx_dryout.volume_db-=delta*2
		if $sfx_dryout.volume_db<=0:
			$sfx_dryout.stop()
		


func _physics_process(delta):
#	vel = move_and_slide_with_snap(vel,Vector3(0,-1,0), Vector3(0,1,0),true)
#	vel = move_and_slide(vel,Vector3(0,-1,0),true)
	vel = move_and_slide(vel,Vector3.UP,false)

	vel.y += G*delta

func get_ground():
	var ground = false
	if $checkground.is_colliding():
		ground = true
		onground = $checkground.get_collider()
		current_ground_type = get_ground_type(onground)
#		floor_kind = onground.get_meta("floor_kind")
#		print("floor kind is: ",floor_kind)
	elif is_on_floor():ground = true
	else:current_ground_type = "air"
	

#	print("walking on ",current_ground_type)
	return ground

func extract_ground_type(obj):
	var o_name = obj.name
	var type = "base"
	if o_name.findn("ground-grass") != -1:
		type = "grass"
	if o_name.findn("ground-cobble") != -1:
		type = "cobble"
	if o_name.findn("ground-metal") != -1:
		type = "metal"
	obj.set_meta("ground_type",type)

func get_ground_type(obj):
	if !obj.has_meta("ground_type"):
		extract_ground_type(obj)
	return obj.get_meta("ground_type")

func get_step_sfx():
	if !groundsfx_db.has(current_ground_type):
		return "step_base"
	else:
		return groundsfx_db[current_ground_type]
#	return "step_base"

func switcharm():#aniamtion control for arms
	var flip = false#flip is always set false for each animation request but...
	match animarm_new:
		"runside_l":#we use a single animation for both left/right (runside), runside_l and runside_r don't actually exist
			flip = true
			animarm_new = "runside"
		"runside_r":
			animarm_new = "runside"
		"punch-combo2":
			flip = true
			animarm_new = "punch-combo1"
	$ui/arms/sprite.flip_h = flip
	$ui/arms/sprite.play(animarm_new)
	animarm = animarm_new
