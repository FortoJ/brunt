#tool
extends KinematicBody

const G = -50
var SPEED = 3
var anim = ""
var anim_new = "idle"

var face_dir = 0
var at_left = false

var ld = Vector3()
var lv = Vector3()


const dir_db = [
			["fnsd",true]		#0	→
			,["sd",true]		#1	↗
			,["bksd",true]		#2	↑
			,["bk",false]		#3	↖	(mirror)
			,["bksd",false]		#4	←	(mirror)
			,["sd",false]		#5	↙	(mirror)
			,["fnsd",false]		#6	↓
			,["fn",false]		#7	↘
]
func _process(delta):
	var new_face = get_face()

#	if face_dir != new_face:
#		face_dir = new_face
#		switch_anim()
#	elif anim != anim_new:
#		switch_anim()
#
	if str(face_dir,anim) != str(new_face,anim_new):
		face_dir = new_face
		switch_anim()

func _physics_process(delta):
	lv = move_and_slide(lv, Vector3.UP)
	lv.y += delta * G

func get_face():
	var cam_dir = $face_dir.global_transform.origin.direction_to(base.cam_pos)*Vector3(1,0,1)#get rotation relative to camera
	$sprite.transform.basis =Transform().looking_at(-cam_dir,Vector3.UP).basis

	var mybas= $face_dir.global_transform.basis
	
	var dir = Vector2(mybas.z.dot(base.cam_basz),mybas.x.dot(base.cam_basz)).normalized().angle()

#	dir = (rad2deg(dir)+180)/360
#	return int(clamp(dir*8,0,7))

	dir = ((rad2deg(dir)+180)/360)+.95
	dir-=int(dir)
	return int(clamp(dir*8,0,7))




#func get_face_bk():
#	var my_dir = ($sprite.rotation.y+3.14)/6.28#get own rotation, convert to float 0.0~1.0
#	var cam_dir = $sprite.global_transform.origin.direction_to(base.cam_pos)#get rotation relative to camera
#	cam_dir = (Vector2(cam_dir.x,cam_dir.z).angle()+3.14)/6.28#convert to float 0.0~1.0
#	var direction = my_dir+cam_dir+.6#sum both rotations, add slight rotation
#	return int((direction-int(direction))*8)#convert to float 0.0~1.0, then integer split in 8 directions
#
#func get_face_bk2():
#	var my_dir = ($face_dir.rotation.y+3.14)/6.28#get own rotation, convert to float 0.0~1.0
#	var cam_dir = $face_dir.global_transform.origin.direction_to(base.cam_pos)*Vector3(1,0,1)#get rotation relative to camera
#
#	$sprite.transform.basis =Transform().looking_at(-cam_dir,Vector3.UP).basis
#
#	cam_dir = (Vector2(cam_dir.x,cam_dir.z).angle()+3.14)/6.28#convert to float 0.0~1.0
#	var direction = my_dir+cam_dir+.6#sum both rotations, add slight rotation
#
#	return int((direction-int(direction))*8)#convert to float 0.0~1.0, then integer split in 8 directions
#
#
#func get_face():
#	var tocam =  $face_dir.global_transform.origin.direction_to(base.cam_pos)*Vector3(1,0,1)#get rotation relative to camera
#	$sprite.transform.basis =Transform().looking_at(-tocam,Vector3.UP).basis
#	var my_dir = $face_dir.rotation_degrees.y#get my degree
#	var cam_dir = $face_dir.global_transform.origin
#	cam_dir = [Vector2(base.cam_pos.x, base.cam_pos.z),Vector2(cam_dir.x,cam_dir.z)]
#	cam_dir = rad2deg(cam_dir[1].angle_to_point(cam_dir[0]))+180#degree relative to world position
#	var face_dir = (my_dir+cam_dir+22)/360#additionate both degree (entity face direction relative to camera)
#	face_dir -= int(face_dir)#remove the number of turns and keep only the current rotation value
#	return int(face_dir*8)#split the float rotation in 8 direction and get only it's integer

func switch_anim():
	var to_left = dir_db[face_dir][1]
	if at_left != to_left:
		var sprite_scale = $sprite.scale.y
		$sprite.flip_h = to_left
		at_left = to_left
	$sprite.animation = str(anim_new+"_"+dir_db[face_dir][0])
	anim=anim_new
	
