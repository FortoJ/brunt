extends "res://system/statemachine_base.gd"

onready var sprite = owner.get_node("sprite")
onready var face_dir = owner.get_node("face_dir")

func physics(delta):
	var vel = owner.lv as Vector3
	machine.base_input()

	if owner.follow_target == null:
		machine.set_state("idle")
		vel = Vector3()
		owner.lv = vel
		return
	var direction = (owner.follow_target-owner.global_transform.origin)
	vel = direction.normalized()*owner.SPEED*delta


	var rot = Vector2(vel.z,vel.x).angle()
#	sprite.rotation.y = lerp_angle(sprite.rotation.y,rot,0.1)
	face_dir.rotation.y = lerp_angle(face_dir.rotation.y,rot,0.1)
	owner.my_hdir = Vector2(vel.z,vel.x).normalized()


	if direction.length() <1:
		owner.follow_target = null
	elif owner.is_on_floor():
		owner.follow_target = null
	elif owner.is_on_wall():
		owner.follow_target = null
	elif owner.is_on_ceiling():
		owner.follow_target = null
		
	owner.lv = vel
#

func clamp_vec3(v, n_max):
	var vx = v
	var vy = v
	var n = sqrt(pow(vx,2) + pow(vy,2))
	var f = min(n, n_max) / n
	return [f * vx, f * vy]
