extends "res://system/statemachine_base.gd"

const slightlyup = Vector3(0,0.1,0)

var ray = null

#func physics(delta):return delta
#func logic(delta):return delta
#func handle_input(ev):return ev


func entering(state_old):
	var ground = get_ground()
	base.place_locator(ground, Color(2,0,0,1))
	if ground != null:
		call_deferred("go_hover",ground)

	else:
		machine.set_state("idle")

func go_hover(ground):
	var hover = get_hover(ground)

	machine.set_state("idle")
	owner.follow_target = hover
		
func get_ground(hoverpoint = null):
	if ray != null:ray.queue_free()
	ray = RayCast.new()
	base.WORLD.add_child(ray)
	if hoverpoint == null:
		ray.global_transform.origin = owner.global_transform.origin
	else:
		ray.global_transform.origin = hoverpoint
	ray.cast_to = Vector3(0,-40,0)
	ray.enabled = true
	ray.add_exception(owner)
	ray.force_raycast_update()
	if ray.is_colliding():
		var collision = ray.get_collision_point()
		collision.y += owner.bodysize*0.5#since we can't squish down the ground, we assume half of body size is up
		return collision
	else: return null#there's no ground!


func get_hover(basepoint):
	if ray != null:ray.queue_free()


	ray = RayCast.new()
	base.WORLD.add_child(ray)

	ray.global_transform.origin = basepoint
	ray.cast_to = Vector3(0,owner.hover_height,0)
	ray.enabled = true
	ray.add_exception(owner)
	ray.force_raycast_update()




	if ray.is_colliding():
		basepoint = ray.get_collision_point()
	else:
		basepoint.y+=owner.hover_height
	basepoint.y-=owner.bodysize*0.5
	base.place_locator(basepoint, Color(0,1,0,1))

	
	return basepoint
#		base.place_locator(basepoint)
#		print("no roof, head to: ",basepoint)
#		return null
#	print("collider is: ",ray.get_collider().name)
