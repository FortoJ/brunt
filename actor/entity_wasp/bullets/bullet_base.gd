extends Area

const POWER_DMG = 1
const SPEED = 10
const RANGE_EXP = 1
var dest = Vector3()
var direction = Vector3()
var parent
var data = {}

func _enter_tree():
	connect("body_entered",self,"hit")
	data = {"power":POWER_DMG
			,"attacker":owner
#			,"SPEE":POWER_DMG
			}

func _ready():
	monitoring = true
#func _ready():
#	global_transform.origin = parent.global_transform.origin
#	pass
func _process(delta):
#	origin+=
	global_transform.origin+=direction*SPEED*delta


func hit(who):
	if who == parent:return
	if who.has_method("be_hit"):
		who.be_hit(data)
	queue_free()
