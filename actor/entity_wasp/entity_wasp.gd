#tool
extends KinematicBody

const G = -50
var SPEED = 200
var hover_height = 6
var anim = ""
var anim_new = "idle"

var face_dir = 0
var at_left = false

var ld = Vector3()
var lv = Vector3()

var follow_target = null

var bodysize

var bullets = {}
var ai
#const dir_db = [
#			["sd",false]		#0	→
#			,["fnsd",false]		#1	↗
#			,["fn",false]		#2	↑
#			,["fnsd",true]		#3	↖	(mirror)
#			,["sd",true]		#4	←	(mirror)
#			,["bksd",true]		#5	↙	(mirror)
#			,["bk",false]		#6	↓
#			,["bksd",false]		#7	↘
#]


const dir_db = [
			["fn",false]		#0	→
			,["fnsd",false]		#1	↗
			,["sd",false]		#2	↑
			,["bksd",false]		#3	↖	(mirror)
			,["bk",false]		#4	←	(mirror)
			,["bksd",true]		#5	↙	(mirror)
			,["sd",true]		#6	↓
			,["fnsd",true]		#7	↘
]

#const dir_db = [
#			["fn",false]		#0	→
#			,["fnsd",true]		#1	↗
#			,["sd",true]		#2	↑
#			,["bksd",true]		#3	↖	(mirror)
#			,["bk",false]		#4	←	(mirror)
#			,["bksd",false]		#5	↙	(mirror)
#			,["sd",false]		#6	↓
#			,["fnsd",false]		#7	↘
#]

var my_hdir = Vector2()
var cam_hdir = Vector2()

func set_weapons():
	bullets["base"] = preload("res://actor/entity_wasp/bullets/bullet_base.tscn")
#	print(bullets)
func _enter_tree():
	ai = $ai
	bodysize = $body.shape.radius*2
	set_weapons()

func _process(delta):
	var new_face = get_face()

#
	if str(face_dir,anim) != str(new_face,anim_new):
		face_dir = new_face
		switch_anim()
func _physics_process(delta):
	lv = move_and_slide(lv, Vector3.UP)


func get_face():
	var trasf = $sprite.global_transform.looking_at(base.cam_pos,Vector3.UP)
	$sprite.global_transform = trasf
	var mybas= $face_dir.global_transform.basis
	var dir = Vector2(mybas.z.dot(base.cam_basz),mybas.x.dot(base.cam_basz)).normalized().angle()

	dir = ((rad2deg(dir)+180)/360)+.95
	dir-=int(dir)
	return int(clamp(dir*8,0,7))

#	dir = (rad2deg(dir)+180)/360
#	return int(clamp(dir*8,0,7))

#
#
#func get_dir2me(dest):
#	var me = $sprite.global_transform.origin
#	if typeof(dest) == 7:dest.y = dest.z
#	return Vector2(me.x-dest.x,me.z-dest.y).normalized()
#
	
#	return Vector2(Vector2(base.cam_pos.x,base.cam_pos.z)-dest).normalized()

	

func switch_anim():
	var to_left = dir_db[face_dir][1]
	if at_left != to_left:
		var sprite_scale = $sprite.scale.y
		$sprite.flip_h = to_left
		at_left = to_left
	$sprite.animation = str(anim_new+"_"+dir_db[face_dir][0])
	anim=anim_new
	

func hit_as_enemy():
	$body.disabled = true
	$StateMachine.set_state("die")
