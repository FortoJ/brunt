extends "res://system/statemachine_base.gd"

var phase
var return_phase

var delay = 0.5
#1walk towards player until sight is lost

var offender_target
var rage = 1

var ray_tester
var rays2check = []
#const rt_mass_db = [Vector3(),Vector3(-1,0,0),Vector3(1,0,0),Vector3(0,0,-1),Vector3(0,0,1)]
#const rt_mass_db = [Vector3(),Vector3(-1,0,0),Vector3(1,0,0),Vector3(0,0,-1),Vector3(0,0,1)]
#const rt_mass_db = [Vector3(),Vector3(-100,0,0),Vector3(1,0,0),Vector3(0,0,-1),Vector3(0,0,1)]
#const rt_mass_db = [Vector3(0,2,0)#center
#					,Vector3(0,3,0)#top
#					,Vector3(0,1,0)#bottom
#					,Vector3(-1,2,0)
#					,Vector3(1,2,0)]
const rt_mass_db = [Vector3()#center
					,Vector3(0,1,0)#top
					,Vector3(0,-.8,0)#bottom
					,Vector3(-1,0,0)
					,Vector3(1,0,0)]
var mass = []

var spots_to_check = []

func _enter_tree():
	ray_tester = RayCast.new()
	for p in rt_mass_db:
		mass.append(p*owner.bodysize*.5)

func _ready():
	owner.call_deferred("add_child", ray_tester)
	ray_tester.add_exception(owner)
	set_raytester()

func set_raytester():
	var mass_ref = owner.get_node("ray_tester")
	
	for i in range(rt_mass_db.size()):
		var new_point = Position3D.new()
		mass_ref.add_child(new_point)
		var sprite = Sprite3D.new()
		new_point.add_child(sprite)
		sprite.billboard = 1
		sprite.texture = load("res://icon.png")
		new_point.transform.origin = rt_mass_db[i]*owner.bodysize*.5
	
func lazy_proc():
	call(phase)
	yield(get_tree().create_timer(delay), "timeout")
	if phase != "off":call_deferred("lazy_proc")

func idle_phase():pass

func chase():
#	var direction = (offender_target.global_transform.origin-owner.global_transform.origin)
	owner.follow_target = owner.global_transform.origin.linear_interpolate(offender_target.global_transform.origin,0.1)
	
	run_raycheck(offender_target)
	return
#	ray_tester.enabled = true
#	ray_tester.add_exception(offender_target)
#	var home_pos = owner.global_transform.origin
#	var offender_pos = offender_target.global_transform.origin
#	owner.get_node("ray_tester").look_at(offender_pos,Vector3.UP)
#	rays2check = []
#	for p in owner.get_node("ray_tester").get_children():
#		rays2check.append([p.global_transform.origin,offender_pos-home_pos])
#	phase = "raychecks"
#	delay = .1

func fire_bullets():
#	owner.follow_target = owner.global_transform.origin.linear_interpolate(offender_target.global_transform.origin,0.1)
	var offender_pos =offender_target.global_transform.origin
	var mypos = owner.global_transform.origin
	var dir = (offender_pos-mypos).normalized()
	owner.follow_target = mypos+(dir*3)
	if (mypos.y-offender_pos.y) <0.4:
		owner.follow_target.y+=randi()%15+4#
	ray_tester.transform.origin = Vector3()
	ray_tester.cast_to = offender_pos-owner.global_transform.origin
	ray_tester.force_raycast_update()
	var shoot = owner.bullets["base"].instance() as Area
	base.WORLD.add_child(shoot)
	shoot.global_transform.origin = mypos
	shoot.direction = dir
	shoot.parent = owner
#	shoot.dest = offender_pos
	
func decision_director(decision_phase):
	print("decision maker")
	match decision_phase:
		"raychecks":

			if return_phase == "chase":
				if rays2check:
					print("offender is under sight and clear path, chase!")
#					owner.follow_target = offender_target.global_transform.origin
					delay = 2
					phase = "chase"
					owner.follow_target = offender_target.global_transform.origin
				else:
					print("offender is not in a clear path, fire bullets!")
					phase = "fire_bullets"
					delay = 1
		_:
			print("decision asked not clear for: ",decision_phase)



func raychecks():
	if ray_tester.is_colliding():
		phase = "raychecks"
		rays2check = false
		print("is colliding, can't chase")
		decision_director(phase)
		return

	rays2check.remove(0)
	if rays2check.size() <=0:#no collisions, we can report the road is clear
		phase = "raychecks"
		rays2check = true
		print("all test done, no collision: clear spot")
		decision_director(phase)
		return

	ray_tester.global_transform.origin = rays2check[0][0]
	ray_tester.cast_to = rays2check[0][1]


func run_raycheck(target =null):#, result = null):
	
	if target == null:
		return_phase = phase
		ray_tester.enabled = false
		ray_tester.clear_exceptions()
		delay = 0.5
		print("init old stuff")
		return


	ray_tester.enabled = true
	ray_tester.add_exception(owner)
	ray_tester.add_exception(target)

	var home_pos = owner.global_transform.origin
	var offender_pos = offender_target.global_transform.origin
	owner.get_node("ray_tester").look_at(offender_pos,Vector3.UP)
	rays2check = []
	for p in owner.get_node("ray_tester").get_children():
		rays2check.append([p.global_transform.origin,offender_pos-home_pos])
	delay = .1

	ray_tester.global_transform.origin = rays2check[0][0]
	ray_tester.cast_to = rays2check[0][1]
	return_phase = phase
	phase = "raychecks"

func chase_bk():
	owner.follow_target = null
	ray_tester.enabled = true
#	var offender_pos = offender_target.global_transform.origin+Vector3(0,owner.bodysize*0.5,0)
	ray_tester.add_exception(offender_target)
	var home_pos = owner.global_transform.origin
	var offender_pos = offender_target.global_transform.origin#-home_pos
	var dir = owner.global_transform.looking_at(offender_pos,Vector3.UP).basis
	
	spots_to_check =[]
	for i in range(mass.size()):
		spots_to_check.append((mass[i])+offender_pos)
#		base.place_locator(spots_to_check[i],Color(2,0,0,.5))
	owner.get_node("ray_tester").look_at(offender_pos,Vector3.UP)

#func physics(delta):return delta
#func logic(delta):call(phases[phase],delta)
#func handle_input(ev):return ev
func entering(state_old):
	if offender_target == null:
		owner.ai.set_state("patrol")
		return
	phase = "chase"
	ray_tester.enabled = true
	lazy_proc()

func off():
	phase = 0
func exiting(state_next):
	ray_tester.enabled = false
	off()
#func anim_finish(anim):pass

