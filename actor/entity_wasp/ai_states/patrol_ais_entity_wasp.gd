extends "res://system/statemachine_base.gd"

var phase = 0
var phases = ["find_where_to_go","checkfreespot","wait_idle"]

var ray_tester
#var detect = null

#
#func _ready():
#	owner.call_deferred("add_child", ray_tester)
#	ray_tester.add_exception(owner)
#	yield(get_tree(), "idle_frame")
#

#phase patrol

#1.shot a cord in radious around you

#2 shot raycast, if not visible (raycast collide with something) go step 1;
#otherwise go 3

#3 shot raycast ground, if raycast DO NOT collide it mean there's no ground:
#use cord generate in 1 and go there; otherwise 4

#4 THERE IS ground (raycast collide), take the ground colliding point and shot
#another raycast upward. Eithier take the upward collision point or the raycast
#max height (if no collision). With the new target go to point #5

#5 shot a raycast from entity to the new target, if not visible (raycast collide)
# return to the target point obtained in #2 and let the entity walk there.
#otherwise let the entity walk to the last target found (here in #5

#variation: suspicious patrol
#if the player is nearby, the 

func _enter_tree():
	ray_tester = RayCast.new()
func _ready():
	owner.call_deferred("add_child", ray_tester)
	ray_tester.add_exception(owner)
	ray_tester.enabled = false
func logic(delta):call(phases[phase])

func nothing():pass
func find_where_to_go():
	ray_tester.enabled = true
	ray_tester.transform.origin = Vector3()
#	var dest = 
	ray_tester.cast_to =machine.cord(Vector3(),20)
#	ray_tester.cast_to =Vector3(0,-100,0)
#	base.place_locator(owner.global_transform.origin+ray_tester.cast_to)
	phase = 1

func checkfreespot():

	if ray_tester.is_colliding():
		phase=0
		return
	ray_tester.global_transform.origin = owner.global_transform.origin+ray_tester.cast_to
	ray_tester.cast_to = Vector3(0, -100,0)
	yield(get_tree(), "idle_frame")
	if !ray_tester.is_colliding():#no ground, just go there
		phase = 0
		return
	ray_tester.global_transform.origin = ray_tester.get_collision_point()+Vector3(0,owner.bodysize*0.5,0)
	ray_tester.cast_to = Vector3(0, owner.hover_height,0)
	yield(get_tree(), "idle_frame")
	var dest = Vector3()
	if !ray_tester.is_colliding():
		dest = ray_tester.global_transform.origin+Vector3(0,owner.hover_height,0)
	else:
		dest = ray_tester.get_collision_point()-Vector3(0,owner.bodysize*0.5,0)
	ray_tester.global_transform.origin = dest
	ray_tester.cast_to = owner.global_transform.origin-dest
	if !ray_tester.is_colliding():
		owner.follow_target = ray_tester.global_transform.origin
	phase = 2
	ray_tester.enabled = false
		
func wait_idle():
	if owner.follow_target == null:
		phase = 0




func entering(state_old):
	ray_tester.enabled = true
	phase = 0
	
func exiting(state_next):
	ray_tester.enabled = false
#func anim_finish(anim):pass
