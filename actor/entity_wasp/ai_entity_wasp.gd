extends Node


#phase patrol

#1.shot a cord in radious around you

#2 shot raycast, if not visible (raycast collide with something) go step 1;
#otherwise go 3

#3 shot raycast ground, if raycast DO NOT collide it mean there's no ground:
#use cord generate in 1 and go there; otherwise 4

#4 THERE IS ground (raycast collide), take the ground colliding point and shot
#another raycast upward. Eithier take the upward collision point or the raycast
#max height (if no collision). With the new target go to point #5

#5 shot a raycast from entity to the new target, if not visible (raycast collide)
# return to the target point obtained in #2 and let the entity walk there.
#otherwise let the entity walk to the last target found (here in #5

#variation: suspicious patrol
#if the player is nearby, the 


# radius of the circle
var circle_r = 100
# center of the circle (x, y)
var circle_x = 5
var circle_y = 7



#STATEMACHINE VARIABLES

var state = null setget set_state
var state_old = null

var state_db = {}
onready var parent = get_parent()



func _ready():

	for i in get_children():
		_add_state(i.name)
	call_deferred("set_state","patrol")
#	call_deferred("set_state","debug")

func cord(pos,radius):
	if typeof(pos) == TYPE_VECTOR2:
		pos = Vector3(pos.x,0,pos.y)
	var alpha = 2 * PI * randf()
# random radius
	var r = radius * sqrt(randf())
	pos.x = r * cos(alpha) + pos.x
	pos.z = r * sin(alpha) + pos.z
	return pos



#statemachine


#initiate machine
func _add_state(state_add):
	state_db[state_add] = get_node(state_add)
	



#processes
func _input(event):
	if state != null:
		state_db[state].handle_input(event)

func _physics_process(delta):
	if state != null:state_db[state].physics(delta)

func _process(delta):
	if state != null:state_db[state].logic(delta)

func anim_finished():
	if state != null:state_db[state].anim_finish(owner.anim)


#switcher
func set_state(new_state):
	if state == new_state:return
	state_old = state
	state = new_state

	if ![state_old,state].has(null):
		state_db[state].entering(state_old)
		state_db[state_old].exiting(state)


