tool
extends Node

const G = -50
var cam_current
var cam_pos = Vector3()
var cam_basz = Vector3()
var screen_size
const base_size = Vector2(1024,600)
var current_ratio = Vector2(1,1)

#overall setting
var MOUSE_SPEED =0.004


#audio
var aud_voice = 0.5
var aud_sfx = 0.5
var aud_music = 0.5

var WORLD
var current_nav

var ingame_players = {}

#items
var item_num
func _enter_tree():
	if !get_node("/root").has_node("main"):return
	randomize()
	if !Engine.editor_hint:WORLD = get_node("/root/main")
	screen_size = get_viewport().size
	get_tree().get_root().connect("size_changed", self, "screen_resized")
	randomize()

func screen_resized():
	screen_size = get_viewport().size
	current_ratio = screen_size/base_size
	print("current ratio is: ",current_ratio)

func _process(delta):
	if Engine.editor_hint:return
	if cam_current == null:
		cam_current = get_viewport().get_camera()
		return
	cam_basz = -cam_current.global_transform.basis.z
	cam_pos = cam_current.global_transform.origin-Vector3(-0.05,0,.05)

func vfx(pos,type = null,color = null):
	var vfx = load("res://graphics/vfx/hityellow1.tscn").instance()
	get_node("/root/main").add_child(vfx)
	if color != null:vfx.modulate = color
	vfx.global_transform.origin = pos
func place_locator(pos,color = null):
	var locator = load("res://system/tools/locator.tscn").instance()
	get_node("/root/main").add_child(locator)
	if color != null:locator.modulate = color
	locator.global_transform.origin = pos


func place_num(pos,num):
	if item_num != null:
		item_num.queue_free()
		item_num = null
	item_num = load("res://graphics/vfx/label.tscn").instance()
	item_num.num = num
	get_node("/root/main").add_child(item_num)
	item_num.global_transform.origin = pos
